const fastify = require('fastify')({ logger: true })
const helmet  = require('@fastify/helmet');
const config  = require('./config');
const gql     = require('mercurius');

const schema  = require('./data/schema');
const resolvers = require('./data/resolvers');

fastify.register(helmet)
  .register(require('@fastify/sensible'))
  .register(require('fastify-healthcheck'))
  .register(require('@fastify/formbody'))
  .register(require('fastify-knexjs'), {
    client: 'mysql',
    debug: (config.get('env') === 'development'),
    connection: config.get('db') 
  })
  .register(require('@fastify/jwt'), {
    secret: config.get('jwtSecret')
  });
fastify.register(gql, { schema });

fastify.route({
  method: 'POST',
  url: '/gql',
  preHandler: (request, reply, done) => {
    if (request.headers.authorization) {
      const token = fastify.jwt.decode(request.headers.authorization.split(' ')[1]);

      if (token) {
        fastify.knex('users').where('id', +token.id).andWhere('pw_version', +token.v).limit(1).then(users => {
          request.user = users[0] || null;
          done();
        })
        .catch(err => {
          request.log.error({message: err.message, stack: err.stack, severity: 'Error'}, 'Error in loading user from JWT');
          done(err);
        });
      } else {
        request.user = null;
        done();
      }
    } else {
      request.user = null;
      done();
    }
  },
  handler: async (request, reply) => {
    const query = request.body.query;
    if (process.env.NODE_ENV === 'development') {
      request.log.info({msg: 'Got a request', payload: query});
    }

    const cxn = {
      ip: request.ip,
      ips: request.ips,
      host: request.headers.host,
      client: request.headers['user-agent']
    };
    // Second param is gql context
    return fastify.graphql(query, { cxn, user: request.user, auth: Boolean(request.user) });
  }
});

const start = async () => {
  try {
    await fastify.listen({port: config.get('port')});
    fastify.log.info(`GraphQL server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

start();
