const typeDef = `
type User {
  id: ID!
  jwt: String
  first_name: String
  last_name: String
  name: String
  email: String
  admin: Boolean 
  last_login: String
  avatar: String
  country: String
  missingFields: [String!]
}
`;

module.exports = typeDef;
