const typeDef = `
type Login {
  last_login: String
  success: Boolean
  ip: String
  client: String
  client_version: String
}
`;

module.exports = typeDef;
