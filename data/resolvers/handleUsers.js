async function getUsers(args, context) {
  const { knex: db, log, user } = context; 

  const query = db('users');

  if (!user.admin) {
    query = query.where('enabled', true);
  }

  log.info(`User(ID: ${user.id}) loaded all users. Admin: ${user.admin}`);
  return await query;
}

async function getUser(args, context) {
  const { knex: db, log, user } = context; 
  const { id } = args;
   
  log.info(`User(ID: ${user.id}) loaded user record(ID: ${id}). Admin: ${user.admin}`);
  return (await db('users').where('id', +id).limit(1))[0];
}

module.exports = {
  getUsers,
  getUser
};

