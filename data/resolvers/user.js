function getMissingFields(user) {
  // These fields are considered 'necessary' for core functionality of ForFans to work
  const fields = ["first_name", "last_name"];
  return [...new Set(fields.filter(field => !(user[field] && user[field].toString().length)).map(field => field.replace(/(fir|la)st_/, '')))];
}

const resolvers = {
  name: (userObj) => {
    return `${userObj.first_name} ${userObj.last_name}`.trim();
  },
  last_login: (userObj, args, context) => {
    const { knex: db } = context.app;

    return db('logins')
      .where('user_id', userObj.id)
      .andWhere('success', true)
      .orderBy('at', 'desc')
      .then(results => results[0]);
  },
  missingFields: (userObj) => {
    return getMissingFields(userObj); 
  }
};

module.exports = resolvers;
