function error(msg = 'Unauthorized Request!') {
  throw new Error(msg);
}

//This resolver scaffold disallows non-authenticated users
async function authed(args, context, cb) {
  if (context.auth) {
    return await cb(args, context);
  } else {
    error();
  }
}

//This resolver scaffold disallows non-authenticated, non-admin users
async function admin(args, context, cb) {
  const { auth, user } = context;
  
  if (auth && user.admin) {
    return await cb(args, context);
  } else {
    error();
  }
}

//This resolver scaffold disallows users who do not also own the specified id
async function owned(args, id, context, cb) {
  const { user, auth } = context;

  if (auth && (user.admin || user.id === +id)) {
    return await cb(args, context);
  } else {
    error();
  }
}

module.exports = {
  authed,
  owned,
  admin,
  error
};
