const bcrypt = require('bcrypt');
const { normalize } = require('../utils');

async function logLogin(db, user_id, success, cxn) {
  const payload = { 
    user_id,
    success, 
    ip: cxn.ip ? cxn.ip : cxn.host,
    client: cxn.client,
    client_version: '' //Write custom logic to parse this out of your User-Agent strings
  };

  await db('logins').insert(payload);
}

async function handleLogin(account, password, context, cxn) {
  const { knex: db, jwt, log, user, auth } = context; 

  if (auth) {
    return user;
  } else {
    // Check to see if it's a username or an e-mail address  
    const payload = (/@/.test(account)) ? { email: account } : { username: account };

    const userResults = await db('users').where(payload).limit(1);

    if (userResults && userResults.length) {
      const user = userResults[0];

      if(bcrypt.compareSync(password, user.digest)) {
        // Valid user    
        await logLogin(db, user.id, true, cxn);

        //w - write permissions (aka: admin)
        user.jwt = jwt.sign({id: user.id, w: user.admin, v: user.pw_version});
        return user;
      } else {
        await logLogin(db, user.id, false, cxn);
        return new Error("Invalid credentials");
      }
    } else {
      return new Error("Invalid credentials");
    }
  }
}

async function handleSignup(args, context, cxn) {
  const { username, email, password } = args;
  const { knex: db, jwt, log, user, auth } = context.app; 

    if (auth) {
      log.info(`User ${user.id} tried to signup while logged in.`);
      return user;
    } else {
      //No @'s in usernames
      if (/@/.test(username)) {
        return new Error("Username cannot contain @ symbol");
      }

      if (username.length && email.length) {
        // First, check to see if email exists in db
        const exists = Boolean((await db('users').where('email', email).limit(1)).length);

        if (exists) {
          log.error("Tried to sign up with duplicate e-mail.", email);
          return new Error("Email already exists.");
        }

        const digest = bcrypt.hashSync(password, 10);
        const [ nUsername, nEmail ] = normalize([username, email]);
        const newUser = {
          username: nUsername, 
          email: nEmail,
          digest,
          pw_version: 1,
          enabled: true
        };
        const user = await db('users').insert(newUser).returning('*');

        await logLogin(db, user.id, true, cxn);
        //w - write permissions (aka: admin)
        user.jwt = jwt.sign({id: user.id, w: false, v: user.pw_version});
        return user;
      } else {
        log.error("Tried to sign up with 0-length username and/or email");
        return new Error("Please provide a username and e-mail address.");
      }
    }
}

module.exports = {
  handleLogin,
  handleSignup
};
