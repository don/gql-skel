const { authed, admin, owned, error }        = require('./scaffold');

const { getUsers, getUser }                   = require('./handleUsers');
const { handleLogin, handleSignup }           = require('./handleAccounts');

const queries = {
  users: async (root, args, context) => {
    return await authed(args, context, getUsers);
  },
  user: async (root, args, context) => {
    return await owned(args, args.id, context, getUser);
  },
};

const mutations = {
  login: async (root, { account, password }, context) => {
    return await handleLogin(account, password, context.app, context.cxn);
  },
  signup: async function (root, args, context) {
    return await handleSignup(args, context.app, context.cxn);
  },
};

module.exports = { queries, mutations };
