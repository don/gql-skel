const { DateTime } = require("luxon");

function now() {
  return DateTime.toISO();
}

function normalize(values) {
  if (Array.isArray(values)) {
    return values.map(value => value.toLowerCase().trim());
  }

  // A JS Object will return an array of key values and have length undefined
  if (Object.keys(values).length && values.length === undefined && typeof values === 'object') {
    Object.keys(values).forEach(key => {
      values[key] = values[key].toLowerCase().trim();
    });

    return values;
  }

  // It's not an array, it's not an object, it will be treated like a string
  try {
    return values.toLowerCase().trim();
  } catch(err) {
    return "";
  }
}

function slug(text) {
  return text.trim().replace(/[^A-Za-z0-9 ]/, '').replace(' ', '-').toLowerCase();
}

function formatCurrency(number, currency) {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency,
  }).format(number);
}

module.exports = {
  now,
  normalize,
  slug,
  formatCurrency
};

