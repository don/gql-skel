const { makeExecutableSchema } = require('@graphql-tools/schema');
const typeDefs = require('./types');
const resolvers = require('./resolvers');

const schema = `
  type Query {
    users: [User]
    user(id: ID!): User
  }
  type Mutation {
    login(account: String!, password: String!): User
    signup(email: String!, password: String!, username: String!): User
  }
`;

module.exports = makeExecutableSchema({ typeDefs: [ schema, ...typeDefs ].join(''), resolvers });
