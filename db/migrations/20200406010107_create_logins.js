exports.up = function(knex, Promise) {
  return knex.schema.createTable('logins', t => {
    t.increments().primary();
    t.integer('user_id').references('users.id');
    t.boolean('success').defaultTo(false);
    t.string('client');
    t.string('client_version');
    t.string('ip');
    t.timestamp('at').defaultTo(knex.fn.now());
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('logins'); 
};
