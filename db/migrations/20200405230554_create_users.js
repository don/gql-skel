exports.up = function(knex, Promise) {
  return knex.schema.createTable('users', t => {
    t.increments().primary();
    t.string('email').notNullable().unique();
    t.string('first_name');
    t.string('last_name');
    t.string('digest').notNullable();
    t.integer('pw_version').defaultTo(1).notNullable();
    t.boolean('enabled').notNullable().defaultTo(true);
    t.boolean('admin').defaultTo(false);
    t.timestamps(true, true);
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('users'); 
};
