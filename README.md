# GraphQL Skeleton

This is a highly-opinionated skeleton for running a [GraphQL](https://graphql.org) server based on the framework [Fastify](https://fastify.io). It is developed and maintained by [Don Burks](https://donburks.com) for use in all levels of projects.

## Installation

1. Clone the repo into a local directory
2. Run `npm install`. 
3. Run `npm start`.
4. Profit

## Package Dependencies

Here are some of the packages that are included in this skeleton:

1. bcrypt - For password hashing
2. convict - For config management
3. dotenv - Environment management
4. fastify - Core web framework
5. fastify-cors - For handling CORS requests
6. fastify-formbody - For pulling data out of the request body. Important for GraphQL queries
7. fastify-healthcheck - Gives a `/health` endpoint for uptime/heartbeat monitoring
8. fastify-helmet - Web security
9. fastify-jwt - For signing and verifying JSON Web Tokens, used for API authetication
10. fastify-knex - For accessing datastore
11. fastify-sensible - Adds some sensible enhancements to Fastify
12. graphql-tools - For schema generation
13. luxon - Time/Date management library
14. mercurius - Graphql module for Fastify
15. pg - Postgres module for Node. (Replace with another module if you are using another datastore)

## Conventions

`/data` - This is the top-level directory for business logic. All of the GraphQL-specific logic is under this directory. Schema is contained in `/data/schema.js`.

`/db` - Database-specific logic here, such as migrations, seeds, etc. 

`/config.js` - Configuration file, using `convict`. 

`/knexfile.js` - Knex-specific configuration. Tightly-coupled to `/config.js`

`/server.js` - Core fastify server file. 

## Types

`/data/types` - Type definitions are stored here. Generally, one file per type definition. 

`/data/types/index.js` - Master types file. Individual type definitions are loaded into this file, and all types are exported from here as a single unit to schema. 

## Resolvers

`/data/resolvers` - All resolver definitions are stored here. 

`/data/resolvers/index.js` - Master resolvers file. All resolvers are loaded into here and exported as a single unit to schema.

`/data/resolvers/main.js` - All queries and mutations are defined here. This uses `/data/scaffold.js` to determine authorization. Individual type-specific resolvers are imported into here and then passed as callback functions to the scaffold. Examples are pre-defined for user queries and login/signup mutations. 

## Scaffold.js

Authorization is managed (mostly) by `/data/resolvers/scaffold.js`. There are three functions exported from that, `authed()`, `owned()`, and `admin()`. Authed assumes that the `auth` flag in GraphQL context is set to true. Owned will accept an `id` parameter to be able to validate that the authed user's ID is the same as the passed ID. This is to validate resource ownership. Admin checks for the `admin` boolean flag on the `user` record. 

### Calling these functions

They are all `async` functions, and take the following parameters:

* args - This is args passed to the GraphQL query
* context - This is the GraphQL query context
* callback - This is the callback function to call if the authorization test passes. Automatically passes `args` and `context` through to the callback, in that order.

Example(data/resolvers/main.js):

```
const { authed } = require('./scaffold');
const { getUsers } = require('./handleUsers');

const queries = {
  users: async (root, args, context, info) => {
    return await authed(args, context, getUsers);
  }
};
```

### Handling errors

Scaffold will throw a new error if authorization fails.
